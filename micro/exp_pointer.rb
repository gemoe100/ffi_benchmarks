require 'ffi'

module Experiment
  ITERATIONS = [100_000]

  def self.setup(n)
    @@buffers = {
      8 => FFI::MemoryPointer.new(:uint8, n),
      16 => FFI::MemoryPointer.new(:uint16, n),
      32 => FFI::MemoryPointer.new(:uint32, n),
      64 => FFI::MemoryPointer.new(:uint64, n)
    }
  end

  def self.run(n)
    [8, 16, 32, 64].each do |b|
      n.times { |i| @@buffers[b].send("put_uint#{b}", i*(b/8), i % 2**b) }
      actual = n.times.map { |i| @@buffers[b].send("get_uint#{b}", i*(b/8)) }
      expected = n.times.map { |i| i % 2**b }
      raise Exception.new("int#{b} read write failed") if actual != expected
    end
  end
end
