require 'ffi'

module CMath
  extend FFI::Library
  ffi_lib 'libm.so'
  [:sin, :cos, :tan, :exp].each { |n| attach_function(n, n, [:double], :double) }
end

module Experiment
  ITERATIONS = [100_000]

  def self.run(n)
    n.times do |i|
      x = n / 1000.to_f
      [:sin, :cos, :tan, :exp].each { |n| CMath.send(n, x) }
    end
  end
end
