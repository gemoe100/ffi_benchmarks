require 'ffi'

module StdIO
  extend FFI::Library
  ffi_lib FFI::Library::LIBC
  attach_function(:sprintf, :sprintf, [:pointer, :string, :varargs], :int)
end

module Experiment
  ITERATIONS = [100_000]

  def self.setup(n)
    @@word = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    @@buf = FFI::MemoryPointer.new(:char, 30)
  end

  def self.run(n)
    n.times do |i|
      d = i % 10
      StdIO.sprintf(@@buf, "%d is written: %s", :int, d, :string, @@word[d])
      expect = "#{d} is written: #{@@word[d]}"
      actual = expect.length.times.map { |i| @@buf.get_char(i).chr }.join.strip
      raise Exception.new("expect: #{expect}\nactual: #{actual}") if actual != expect
    end
  end
end
