require 'ffi'

module Stdlib
  extend FFI::Library
  ffi_lib FFI::Library::LIBC
  callback :qsort_cmp, [ :pointer, :pointer ], :int
  attach_function :qsort, [ :pointer, :ulong, :ulong, :qsort_cmp ], :int
end

module Experiment
  ITERATIONS = [50_000]

  def self.run(n)
    unsorted = n.times.to_a.shuffle
    p = FFI::MemoryPointer.new(:int32, n)
    unsorted.each_with_index { |v, i| p.put_int32(4*i, v) }
    Stdlib.qsort(p, n, 4) do |p1, p2|
      i1 = p1.read_int32
      i2 = p2.read_int32
      i1 - i2
    end
    sorted = n.times.map { |i| p.get_int32(4*i) }
    raise Exception.new("sorting failed") if sorted != sorted.sort
  end
end
