require 'ffi'

module Str
  extend FFI::Library
  ffi_lib FFI::Library::LIBC
  attach_function(:cpy, :strcpy, [:pointer, :string], :pointer)
  attach_function(:cat, :strcat, [:pointer, :string], :pointer)
end

module Experiment
  ITERATIONS = [100_000]

  def self.run(n)
    res = FFI::MemoryPointer.new(:char, n)
    Str.cpy(res, 1.chr)
    1.upto(n-1) { |i| Str.cat(res, (i % 255 + 1).chr) }
    n.times do |i|
      j = res.get_uchar(i)
      k = i % 255 + 1
      raise Exception.new("fail: #{j} != #{k}") if j != k
    end
  end
end
