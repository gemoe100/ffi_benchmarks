require 'ffi'

module Num
  extend FFI::Library
  ffi_lib FFI::Library::LIBC
  Sym = enum(:sym_1, 1,
             :sym_2, 2,
             :sym_3, 3,
             :sym_neg1, -1,
             :sym_neg2, -2,
             :sym_neg3, -3)
  attach_function("abs", "abs", [Sym], Sym)
end

module Experiment
  ITERATIONS = [100_000]

  def self.run(n)
    n.times do |i|
      j = (i % 3) + 1
      sym = "sym_neg#{j}".to_sym
      res = Num.abs(sym)
      raise Exception.new("#{res} != sym_#{i}") if res != "sym_#{j}".to_sym
      raise Exception.new("[]-invariant failed") if Num::Sym[Num::Sym[j]] != j
    end
  end
end
