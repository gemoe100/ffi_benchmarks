``micro`` and ``macro`` contain the micro and macro benchmarks, respectively.

``scripts`` contains the scripts which are used to transform the JSON output of
a Ruby experiment to either input for bargraph.pl or latex code for a tabular.

json2graph_logic.rb is the back end for all the other scripts which also have
the prefix json2.
