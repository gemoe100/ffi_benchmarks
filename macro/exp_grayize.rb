require 'ffi'

module Experiment
  ITERATIONS = [1000]

  module G
    extend FFI::Library
    ffi_lib 'gtk-x11-2.0'
    attach_function(:type_init, :g_type_init, [], :void)
  end
  module Gdk
    extend FFI::Library

    Colorspace = enum(:GDK_COLORSPACE_RGB)
    module PixbufMethod
      extend FFI::Library
      ffi_lib 'gdk_pixbuf-2.0'
      attach_function(:get_rowstride, :gdk_pixbuf_get_rowstride,
                      [:pointer], :int)
      attach_function(:get_pixels, :gdk_pixbuf_get_pixels,
                      [:pointer], :pointer)
      attach_function(:new, :gdk_pixbuf_new,
                      [Colorspace, :bool, :int, :int, :int], :pointer)
    end
  end

  def self.setup(n)
    G.type_init()
  end

  def self.run(n)
    pixbuf = Gdk::PixbufMethod.new(:GDK_COLORSPACE_RGB, false, 8, n, n)
    #rowstride = Gdk::PixbufMethod.get_rowstride(pixbuf) # not equal to 3*n
    pixels = Gdk::PixbufMethod.get_pixels(pixbuf)
    #filling up the pixbuf
    n.times do |i|
      n.times do |j|
          index = (j*Gdk::PixbufMethod.get_rowstride(pixbuf) + 3*i)
          pixels.put_uchar(index+0, i % 255)
          pixels.put_uchar(index+1, j % 255)
          pixels.put_uchar(index+2, (i+j) % 255)
      end
    end
    # reading from the pixbuf
    n.times do |i|
      n.times do |j|
        index = (j*Gdk::PixbufMethod.get_rowstride(pixbuf) + 3*i)
        red   = pixels.get_uchar(index+0)
        green = pixels.get_uchar(index+1)
        blue  = pixels.get_uchar(index+2)
        raise Exception.new("unexpected red value") if red != i % 255
        raise Exception.new("unexpected green value") if green != j % 255
        raise Exception.new("unexpected blue value") if blue != (i+j) % 255
      end
    end
  end
end
