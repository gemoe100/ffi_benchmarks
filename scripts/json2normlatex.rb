load 'json2graph_logic.rb'

raise ArgumentError.new("no file given, stop.") if ARGV.length == 0
data = File.open(ARGV[0]) { |f| f.gets }

table = Json2Graph.transform(data, "execution_time", "table")
errorbars = Json2Graph.transform(data, "execution_time_deviation", "yerrorbars")
norm_table = table.map { |x, y| [1.0, y/x] }
norm_errorbars = errorbars.map(table) { |a, b, x, y| [a/x, b/x] }

# cut of some precision
norm_table = norm_table.map { |x, y| [x.round(3), y.round(3)] }
norm_errorbars = norm_errorbars.map { |x, y| [x.round(3), y.round(3)] }

hsh = {"table" => "execution_time", "yerrorbars" => "execution_time_deviation"}
puts norm_table.merge(norm_errorbars).to_latex_tabular(hsh)
