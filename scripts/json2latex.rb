load 'json2graph_logic.rb'

info_default = ["execution_time"]
category_default = ["table"]

raise ArgumentError.new("no file given, stop.") if ARGV.length == 0
data = File.open(ARGV[0]) { |f| f.gets }

$stderr.puts "warning: no desired information given, assuming #{info_default} is desired" if ARGV.length < 2
info = if ARGV[1] then ARGV[1].split('|') else info_default end

$stderr.puts "warning: no desired category given, assuming #{category_default} is desired" if ARGV.length < 3
category = if ARGV[2] then ARGV[2].split('|') else category_default end
raise ArgumentError.new("length of first two arguments don't match") if info.length != category.length

bargraphs = info.zip(category).map { |i, c| Json2Graph.transform(data, i, c) }

# cut of some precision
bargraphs = bargraphs.map { |bg| bg.map { |x, y| [x.round(3), y.round(3)] } }

label = info.zip(category).inject({}) { |hsh, arr| hsh.merge({arr[1] => arr[0]}) }
puts bargraphs.first.merge(bargraphs.last).to_latex_tabular(label)
