require 'rspec'
load 'json2graph_logic.rb'

describe Json2Graph do
  it "transforms the experiment results from json to a class representing barpgraph.pl input" do
    input =  <<-jsoncode
             {"ruby-head":
               {"micro/exp_foo.rb":
                 {"1000":{"interesting":10, "rest":-1}},
                "macro/exp_bar.rb":
                 {"1000":{"interesting":20, "rest":-1}}},
              "topaz-head":
               {"micro/exp_foo.rb":
                 {"1000":{"interesting":30, "rest":-1}},
                "macro/exp_bar.rb":
                 {"1000":{"interesting":40, "rest":-1}}}}
             jsoncode
    result = Json2Graph.transform(input, "interesting", "table")
    result.class.should == Bargraph
    result.cluster.should == ["ruby-head", "topaz-head"]
    result.categories.should == ["table"]
    result.experiments.should == ["macro/exp_bar.rb", "micro/exp_foo.rb"]
    result.rawdata == [[[20, 40], [10, 30]]]
  end
end

describe Bargraph do
  describe "#merge" do
    it "merges two bargraphs if their cluster attrs are equal" do
      bg1 = Bargraph.snew(["ruby-head", "topaz-head"],
                          "table",
                          ["macro/exp_bar.rb", "micro/exp_foo.rb"],
                          [[20, 40], [10, 30]])
      bg2 = Bargraph.snew(["ruby-head", "topaz-head"],
                          "yerrorbars",
                          ["macro/exp_bar.rb", "micro/exp_foo.rb"],
                          [[0.1, 0.5], [0.2, 0.4]])
      bg3 = bg1.merge(bg2)
      bg3.cluster.should == bg1.cluster
      bg3.categories.should == ["table", "yerrorbars"]
      bg3.experiments.should == bg1.experiments
      bg3.rawdata.should == [[[20, 40], [10, 30]], [[0.1, 0.5], [0.2, 0.4]]]
    end
  end
  describe "#to_s" do
    it "creates bargraph.pl input" do
      Bargraph.new(["ruby-head", "topaz-head"],
                   ["table", "yerrorbars"],
                   ["macro/exp_bar.rb", "micro/exp_foo.rb"],
                   [[[20, 40], [10, 30]], [[0.1, 0.5], [0.2, 0.4]]]).to_s.should ==
<<-graphcode
=cluster ruby-head topaz-head
=table
macro/exp_bar.rb 20 40
micro/exp_foo.rb 10 30
=yerrorbars
macro/exp_bar.rb 0.1 0.5
micro/exp_foo.rb 0.2 0.4
graphcode
    end
  end
  describe "#to_latex_tabular" do
    it "creates a latex tabular environment" do
      bg1 = Bargraph.new(["Ruby", "Topaz", "Other"],
                         ["table", "yerrorbars"],
                         ["micro/exp_foo", "micro/exp_bar"],
                         [[[15.3, 2.5], [14.2, 6.7]], [[0.1, 0.02], [0.3, 0.07]]])
      bg1.to_latex_tabular({"table"=>"exec-time", "yerrorbars"=>"exec-time-dev"}).should ==
<<-tabular
\\begin{tabular}{c||ccc|ccc}
& exec-time & & & exec-time-dev\\\\
& Ruby & Topaz & Other & Ruby & Topaz & Other\\\\
\\hline
\\hline
micro/exp\\_foo & 15.3 & 2.5 & 0.1 & 0.02\\\\
\\hline
micro/exp\\_bar & 14.2 & 6.7 & 0.3 & 0.07\\\\
\\hline
\\end{tabular}
tabular
    end
  end
  describe "#map" do
    it "applies the given block to each element of the rawdata attr and returns a new Bargraph object" do
      bg1 = Bargraph.new(["ruby-head", "topaz-head"],
                         ["table1", "table2"],
                         ["macro/exp_bar.rb", "micro/exp_foo.rb"],
                         [[[1, 2], [3, 4]], [[5, 6], [7, 8]]])
      bg2 = bg1.map { |x, y| [y, x] }
      bg2.cluster.should == bg1.cluster
      bg2.categories.should == bg1.categories
      bg2.experiments.should == bg1.experiments
      bg2.rawdata.should == [[[2, 1], [4, 3]], [[6, 5], [8, 7]]]
    end 
    it "can also use foreign rawdata from another Bargraph" do
      bg1 = Bargraph.snew(["ruby-head", "topaz-head"],
                          "table",
                          ["macro/exp_bar.rb", "micro/exp_foo.rb"],
                          [[1, 2], [3, 4]])
      bg2 = Bargraph.snew(["ruby-head", "topaz-head"],
                          "factor", # in this test, it doesn't matter wether this is a valid category for bargraph
                          ["macro/exp_bar.rb", "micro/exp_foo.rb"],
                          [[10, 100], [1000, 10000]])
      bg3 = bg1.map(bg2) { |x, y, a, b| [x*a, y*b] }
      bg3.cluster.should == bg1.cluster
      bg3.categories.should == bg1.categories
      bg3.experiments.should == bg1.experiments
      bg3.rawdata.should == [[[10, 200], [3000, 40000]]]
    end
  end
end
