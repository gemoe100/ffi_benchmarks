require 'json'

class Bargraph
  def initialize(cluster, categories, experiments, rawdata)
    @cluster = cluster
    @categories = categories
    @experiments = experiments
    @rawdata = rawdata
  end
  attr_reader :cluster
  attr_reader :categories
  attr_reader :experiments
  attr_reader :rawdata

  def self.snew(cluster, category, experiments, single_rawdata)
    Bargraph.new(cluster, [category], experiments, [single_rawdata])
  end

  def merge(other)
    Bargraph.new(@cluster,
                 self.categories + other.categories,
                 @experiments,
                 self.rawdata + other.rawdata)
  end

  def to_s
    head = "=cluster #{@cluster.join(' ')}"
    strdata = @rawdata.map { |row| row.zip(@experiments).map { |d, e| ([e] + d).join(' ') } }
    body = @categories.map { |c| '=' + c }.zip(strdata).flatten.join("\n")
    "#{head}\n#{body}\n"
  end

  def to_latex_tabular(category_map)
    layout = (['c' * @cluster.length] * @categories.length).join('|')
    row1 = @categories.map { |c| category_map[c] }.join(' ' + '& ' * @cluster.length)
    row2 = (@cluster * @categories.length).join(' & ')
    rest_arr = @experiments.length.times.map { |i| ([@experiments[i]] + @rawdata.map { |a| a[i] }).join(' & ') }
    rest = (rest_arr + ['']).join("\\\\\n\\hline\n")
text = <<-res
\\begin{tabular}{c||#{layout}}
& #{row1}\\\\
& #{row2}\\\\
\\hline
\\hline
#{rest}\\end{tabular}
res
    text.gsub("_", "\\_")
  end

  def map(other=nil, &block)
    new_rawdata = @rawdata.map do |rd|
      rd = if other then rd.zip(other.rawdata.first).map { |l, r| l + r} else rd end
      new_rawdata = rd.map(&block)
    end
    Bargraph.new(@cluster, @categories, @experiments, new_rawdata)
  end
end

module Json2Graph
  def self.transform(json_data, info, category)
    ruby_data = JSON.parse(json_data)
    benchmarks = ruby_data.each_value.to_a.first.keys
    bar_hash_data = benchmarks.inject({}) { |hsh, key| hsh.merge({key => []}) }
    ruby_data.each_value do |h|
      h.each_pair do |k, v|
        value = v.values.first[info]
        bar_hash_data[k] << value
      end
    end
    bar_list_data = bar_hash_data.to_a.sort
    cluster = ruby_data.keys
    experiments = bar_list_data.map { |k, v| k }
    rawdata = bar_list_data.map { |k, v| v }
    Bargraph.snew(cluster, category, experiments, rawdata)
  end
end
