load 'json2graph_logic.rb'

raise ArgumentError.new("no file given, stop.") if ARGV.length == 0
data = File.open(ARGV[0]) { |f| f.gets }

table = Json2Graph.transform(data, "execution_time", "table")
errorbars = Json2Graph.transform(data, "execution_time_deviation", "yerrorbars")
norm_table = table.map { |x, y| [1.0, y/x] }
norm_errorbars = errorbars.map(table) { |a, b, x, y| [a/x, b/x] }
puts norm_table.merge(norm_errorbars).to_s
